import scala.collection.mutable.ArrayBuffer

object Test extends App {

  val k = if (args.nonEmpty) args(0).toInt else 100
  println(calcFloors(k))

  def calcFloors(k: Int) = {
    val result = ArrayBuffer[Int]()
    var i = k
    while (i > 0) {
      i -= Math.ceil((-1.0 + Math.sqrt(1 + 8 * i)) / 2.0).toInt
      result += k - i
    }
    result
  }

}
