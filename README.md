# 2 bulbs and 100 floors puzzle

This exercise is trying to solve following puzzle:

> Given two bulbs, find the highest floor an bulb can be dropped from without breaking, with as few drops as possible.

## Solution
Because there are 100 floors in this problem, to solve it for x when the entire summation is equal to 100:

    x * (x + 1) / 2 = 100
    
Which on solving gives

    x = ceil(−1 + sqrt(1 + 8 * k)) / 2)

where `k` is a number of floors.

## How to run

This `sbt` project and to run application it should be installed.

To run application execute following command:

    > sbt "run 100"

`100` is a number of floors in the building and it can be changed.

The other way to run it to first execute `sbt` and then:

    sbt> run 100
